# The HTML5 Banner Generator #
(Misega)[https://github.com/misega/HTML5-Banners] has a pretty good boilerplate but doesn't concat shared assets, 
(Dipscom)[https://github.com/dipscom/banner-boilerplate] concats banner assets with a shared folder but doesn't have what Misega has.  
This is a banner generator which took features from each. Also I was kinda tired of having to open 45 different tabs when I had to build 5 banner sizes for 9 different designs.

It concats shared assets.    
It zips distributables.  
It generates a page to view compiled banners.  
It gives you controls and links to each compiled banner.  
What more do you want?

## Dependancies ##
- Node / Yarn
- Gulp

## Getting started ##
- `gulp help` to view available commands
- `gulp watch` to compile page to watch all banners. Includes controls as well as links to banners
- `gulp build` builds all banners and zips them to deploy

## Explanation of Structure ##
- Follow the example structure.
- Fallback images are a must. `.png`, `.jpg`, `.gif` are accepted as valid images. 
- Assets in shared will be compiled to the `_dist` folder along with their relative assets.
- `scripts` in shared will be prepended to each banner(s) scripts.
- `images` in shared will be added to each banner(s) `_dist` folder.
- `styles` in shared will be prepended to each banner(s) existing styles.

## Folder Structure ##
``` Markdown
├── _banner-support-files/              # Folder with files for gulp tasks
│   └── index.html                      # Page served during `gulp watch`
│
├── _banners/
│   ├── example__300x250/               # Generic Example remove when no longer in use 
│   │   ├── index.html                  # Banners main HTML file
│   │   ├── config.js                   # ADKit DCO config (delete if not in use)
│   │   ├── fallback.jpg                # Fallback Image, this is required
│   │   └── assets/                     # Folder for Assets
│   │       ├── images/                 # Images (`.jpg`,`.png`,`.gif`)
│   │       ├── scripts/                # Scripts (`.js`)
│   │       └── styles/                 # Images folder (`.scss`)
│   │
│   └── company__300x250/               # Sizmek Example remove when no longer in use 
│       ├── index.html                  # Banners main HTML file
│       ├── config.js                   # ADKit DCO config (delete if not in use)
│       ├── fallback.jpg                # Fallback Image, this is required
│       └── assets/                     # Folder for Assets
│           ├── images/                 # Images (`.jpg`,`.png`,`.gif`)
│           ├── scripts/                # Scripts (`.js`)
│           └── styles/                 # Images folder (`.scss`)
│
├── _shared/                            # Files here are included / concat into each banner
│   ├── images/                         # Images (`.jpg`,`.png`,`.gif`)
│   ├── scripts/                        # Scripts (`.js`)
│   └── styles/                         # Images folder (`.scss`)
│
├── _dist/                              # Distibutables folder
├── _dist-min/                          # Zip Distibutables folder
│
├── gulpfile.js/                        # Gulp logic
├── node_modules/                       # Node stuff
├── .gitignore/                         # Git ignore
├── package.json/                       # NPM stuff
└── yarn.lock/                          # Yarn stuff
```