/* 
  Dom Utilities
  -------------------- */
var DOM_UTILS = {
  getAllIdElements: function (scope) {
    scope = scope || document;
    // returns an array of all elements in scope that have an ID
    var items = scope.getElementsByTagName("*");
    var elements = [];
    var i = 0;
    for (i = 0; i < items.length; i++) {
      if (items[i].hasAttribute("id")) {
        elements.push(items[i]);
      }
    }
    return elements;
  },
  getAllIds: function (scope) {
    scope = scope || document;
    // returns an array of strings of all the id names in scope
    var items = scope.getElementsByTagName("*");
    var ids = [];
    var len = items.length;
    var i = 0;
    for (i = 0; i < len; i++) {
      if (items[i].hasAttribute("id")) {
        ids.push(items[i].id);
      }
    }
    return ids;
  }
}