/***
 *                       _       __    __         
 *     _   ______ ______(_)___ _/ /_  / /__  _____
 *    | | / / __ `/ ___/ / __ `/ __ \/ / _ \/ ___/
 *    | |/ / /_/ / /  / / /_/ / /_/ / /  __(__  ) 
 *    |___/\__,_/_/  /_/\__,_/_.___/_/\___/____/  
 *     
 *  Don't edit unless you know what you're doing
 *                                            
 */

var timeline = new TimelineMax();
var element = {};

/*
  Setup elements for use
  -------------------- */
var t_Elements = DOM_UTILS.getAllIdElements(document.getElementById('main'));

DOM_UTILS.getAllIds(document.getElementById('main')).forEach(function (val, ind) {
  element[val] = t_Elements[ind];
})

/***
 *               __      __  ____                   
 *        ____  / /___ _/ /_/ __/___  _________ ___ 
 *       / __ \/ / __ `/ __/ /_/ __ \/ ___/ __ `__ \
 *      / /_/ / / /_/ / /_/ __/ /_/ / /  / / / / / /
 *     / .___/_/\__,_/\__/_/  \____/_/  /_/ /_/ /_/ 
 *    /_/ 
 * 
 *  Remove unused code from here
 *                                           
 */

window.addEventListener('load', (function () { !EB.isInitialized() ? EB.addEventListener(EBG.EventName.EB_INITIALIZED, onInit) : onInit(); }));

/***
 *        _       _ __ 
 *       (_)___  (_) /_
 *      / / __ \/ / __/
 *     / / / / / / /_  
 *    /_/_/ /_/_/\__/  
 *                     
 */

function onInit() {

  // Setup Clicks
  element['cta'].addEventListener('click', clickThroughHandler);
  element['clicktag'].addEventListener('click', clickThroughHandler);
  element['reload'].addEventListener('click', replayClickHandler);

  // Init timeline
  initTimeline();

  // Show banner
  document.getElementById('main').style.display = 'block';

}

/***
 *       __  _                ___          
 *      / /_(_)___ ___  ___  / (_)___  ___ 
 *     / __/ / __ `__ \/ _ \/ / / __ \/ _ \
 *    / /_/ / / / / / /  __/ / / / / /  __/
 *    \__/_/_/ /_/ /_/\___/_/_/_/ /_/\___/ 
 *                                         
 */

function initTimeline() {
  
  var animTiming = 0.75;
  var animStagger = 0.2;
  var animEasing = Power3.easeInOut;

  timeline
    .add('frame1')
    .to('#frame-1__pink--background', animTiming, {
      width: '50%', ease: animEasing,
      onStart: (function () {
        TweenMax.set('#frame-4__pink--cta', { display: 'block' })
        TweenMax.set('#frame-4__blue--cta', { display: 'block' })
        TweenMax.set('#cta', { display: 'none' })
      })
    }, 'frame1')
    .to('#frame-1__blue--background', animTiming, { width: '50%', ease: animEasing }, 'frame1')

    .add('frame2Pink', '+=1.2')
    .to('#frame-2__pink--background', animTiming, { width: '100%', ease: animEasing }, 'frame2Pink')
    .from('#frame-2__pink--copy-mask', animTiming/2, { width: 0, ease: animEasing }, 'frame2Pink+=' + animStagger)
    .to('#frame-2__pink--copy', animTiming/2, { left: 10, opacity: 1, ease: animEasing }, 'frame2Pink+=' + animStagger)

    .add('frame2Blue', '+=1.2')
    .to('#frame-2__blue--background', animTiming, { width: '100%', ease: animEasing }, 'frame2Blue')
    .from('#frame-2__blue--copy-mask', animTiming/2, { width: 0, ease: animEasing }, 'frame2Blue+=' + animStagger)
    .to('#frame-2__blue--copy', animTiming/2, { right: 10, opacity: 1, ease: animEasing }, 'frame2Blue+=' + animStagger)

    /*
    .add('frame3', '+=1.2')
    .to('#frame-3__pink--background', 0.6, { width: '100%', ease: animEasing }, 'frame3')
    .to('#frame-3__blue--background', 0.6, { width: '50%', ease: animEasing }, 'frame3+=.45')
    */

    .add('frame4', '+=1.2')
    .to('#frame-4__blue--background', animTiming, { width: '100%', ease: animEasing }, 'frame4')
    .to('#frame-4__pink--background', animTiming, {
      width: '50%', ease: animEasing,
      onUpdate: (function () {
        TweenMax.set('#frame-4__pink--cta', { display: 'block' })
        TweenMax.set('#frame-4__blue--cta', { display: 'block' })
        TweenMax.set('#cta', { display: 'none' })
      }),
      onComplete: (function () {
        TweenMax.set('#frame-4__pink--cta', { display: 'none' })
        TweenMax.set('#frame-4__blue--cta', { display: 'none' })
        TweenMax.set('#cta', { display: 'block' })
      })
    }, 'frame4+=' + animStagger)
    .from('#reload', animTiming/2, { autoAlpha: 0 })

  if (timeline.totalDuration() > 15) {
    console.log('Duration is more than 15seconds, scaling down time');
    timeline.timeScale(timeline.totalDuration() / 15);
  }

}

/***
 *        __                    ____              
 *       / /_  ____ _____  ____/ / /__  __________
 *      / __ \/ __ `/ __ \/ __  / / _ \/ ___/ ___/
 *     / / / / /_/ / / / / /_/ / /  __/ /  (__  ) 
 *    /_/ /_/\__,_/_/ /_/\__,_/_/\___/_/  /____/  
 *                                                
 */

function clickThroughHandler(evt) {
  evt.stopPropagation();
  EB.clickthrough();
}

function replayClickHandler(evt) {
  evt.stopPropagation();
  timeline.restart();
}