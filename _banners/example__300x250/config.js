define({
    "SV": {
        "svData": [{
            "svKey": "frame-1a-text",
            "svType": "string",
            "value": "Line One."
        }, {
            "svKey": "frame-1b-text",
            "svType": "string",
            "value": "Line Two."
        }, {
            "svKey": "frame-1c-text",
            "svType": "string",
            "value": "Line Three."
        }, {
            "svKey": "frame-1d-text",
            "svType": "string",
            "value": "Line Four."
        }, {
            "svKey": "frame-2a-text",
            "svType": "string",
            "value": "Line One."
        }, {
            "svKey": "frame-2b-text",
            "svType": "string",
            "value": "Line Two."
        }, {
            "svKey": "frame-2c-text",
            "svType": "string",
            "value": "Line Three."
        }, {
            "svKey": "frame-2d-text",
            "svType": "string",
            "value": ""
        }, {
            "svKey": "frame-3a-text",
            "svType": "string",
            "value": "Line One."
        }, {
            "svKey": "frame-3b-text",
            "svType": "string",
            "value": "Line Two."
        }, {
            "svKey": "frame-3c-text",
            "svType": "string",
            "value": ""
        }, {
            "svKey": "frame-3d-text",
            "svType": "string",
            "value": ""
        }, {
            "svKey": "cta",
            "svType": "string",
            "value": "Buy Now"
        }]
    }
});