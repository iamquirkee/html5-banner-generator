/***
 *                       _       __    __         
 *     _   ______ ______(_)___ _/ /_  / /__  _____
 *    | | / / __ `/ ___/ / __ `/ __ \/ / _ \/ ___/
 *    | |/ / /_/ / /  / / /_/ / /_/ / /  __(__  ) 
 *    |___/\__,_/_/  /_/\__,_/_.___/_/\___/____/  
 *     
 *  Don't edit unless you know what you're doing
 *                                            
 */

var timeline = new TimelineMax();
var element = {};

/*
  Setup elements for use
  -------------------- */
var t_Elements = DOM_UTILS.getAllIdElements(document.getElementById('main'));

DOM_UTILS.getAllIds(document.getElementById('main')).forEach(function (val, ind) {
  element[val] = t_Elements[ind];
})

/***
 *               __      __  ____                   
 *        ____  / /___ _/ /_/ __/___  _________ ___ 
 *       / __ \/ / __ `/ __/ /_/ __ \/ ___/ __ `__ \
 *      / /_/ / / /_/ / /_/ __/ /_/ / /  / / / / / /
 *     / .___/_/\__,_/\__/_/  \____/_/  /_/ /_/ /_/ 
 *    /_/ 
 * 
 *  Remove unused code from here
 *                                           
 */

/*
  Sizmek API
  --------------------*/
// adkit.onReady(onInit);
// window.addEventListener('load', (function () { !EB.isInitialized() ? EB.addEventListener(EBG.EventName.EB_INITIALIZED, onInit) : onInit(); }));

/*
  Google API
  --------------------*/

// window.onload = function () {
//   if (Enabler.isInitialized()) {
//     onInit();
//   } else {
//     Enabler.addEventListener(studio.events.StudioEvent.INIT, onInit);
//   }
// }

/*
  No API
  --------------------*/
window.onload = function () {
  onInit();
}

/***
 *        _       _ __ 
 *       (_)___  (_) /_
 *      / / __ \/ / __/
 *     / / / / / / /_  
 *    /_/_/ /_/_/\__/  
 *                     
 */

function onInit() {

  // Setup Clicks
  element['cta'].addEventListener('click', clickThroughHandler);
  element['clicktag'].addEventListener('click', clickThroughHandler);
  element['reload'].addEventListener('click', replayClickHandler);

  // Init Copies
  initCopies();

  // Init timeline
  initTimeline();

  // Show banner
  document.getElementById('main').style.display = 'block';

}

function initCopies() {
  // Setup Copies
}

/***
 *       __  _                ___          
 *      / /_(_)___ ___  ___  / (_)___  ___ 
 *     / __/ / __ `__ \/ _ \/ / / __ \/ _ \
 *    / /_/ / / / / / /  __/ / / / / /  __/
 *    \__/_/_/ /_/ /_/\___/_/_/_/ /_/\___/ 
 *                                         
 */

function initTimeline() {

  timeline
    .add('init')

  timeline
    .add('start')

  if (timeline.totalDuration() > 15) {
    console.log('Duration is more than 15seconds, scaling down time');
    timeline.timeScale(timeline.totalDuration() / 15);
  }
    
}

/***
 *        __                    ____              
 *       / /_  ____ _____  ____/ / /__  __________
 *      / __ \/ __ `/ __ \/ __  / / _ \/ ___/ ___/
 *     / / / / /_/ / / / / /_/ / /  __/ /  (__  ) 
 *    /_/ /_/\__,_/_/ /_/\__,_/_/\___/_/  /____/  
 *                                                
 */

function clickThroughHandler(evt) {
  evt.stopPropagation();
  EB.clickthrough();
}

function replayClickHandler(evt) {
  evt.stopPropagation();
  timeline.restart();
}