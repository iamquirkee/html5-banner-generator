/***
 *                       _       __    __         
 *     _   ______ ______(_)___ _/ /_  / /__  _____
 *    | | / / __ `/ ___/ / __ `/ __ \/ / _ \/ ___/
 *    | |/ / /_/ / /  / / /_/ / /_/ / /  __(__  ) 
 *    |___/\__,_/_/  /_/\__,_/_.___/_/\___/____/  
 *     
 *  Don't edit unless you know what you're doing
 *                                            
 */

var timeline = new TimelineMax();
var element = {};

/*
  Setup elements for use
  -------------------- */
var t_Elements = DOM_UTILS.getAllIdElements(document.getElementById('main'));

DOM_UTILS.getAllIds(document.getElementById('main')).forEach(function (val, ind) {
  element[val] = t_Elements[ind];
})

/***
 *               __      __  ____                   
 *        ____  / /___ _/ /_/ __/___  _________ ___ 
 *       / __ \/ / __ `/ __/ /_/ __ \/ ___/ __ `__ \
 *      / /_/ / / /_/ / /_/ __/ /_/ / /  / / / / / /
 *     / .___/_/\__,_/\__/_/  \____/_/  /_/ /_/ /_/ 
 *    /_/ 
 * 
 */
window.addEventListener('load', (function () { !EB.isInitialized() ? EB.addEventListener(EBG.EventName.EB_INITIALIZED, onInit) : onInit(); }));

/***
 *        _       _ __ 
 *       (_)___  (_) /_
 *      / / __ \/ / __/
 *     / / / / / / /_  
 *    /_/_/ /_/_/\__/  
 *                     
 */

function onInit() {

  // Setup Clicks
  element['cta'].addEventListener('click', clickThroughHandler);
  element['clicktag'].addEventListener('click', clickThroughHandler);
  element['reload'].addEventListener('click', replayClickHandler);

  // Init SnowFlakes
  initSnowFlakes();

  // Init timeline
  initTimeline();

  // Show banner
  document.getElementById('main').style.display = 'block';

}

function initSnowFlakes() {
  
    var ran = 1000;
    var stutter = 3000;
  
    // Setup SnowFlakes
    setTimeout(function () { newRandomTop('#red-1__holder'); }, (Math.random() * ran) + (stutter * 1));
    setTimeout(function () { newRandomTop('#red-2__holder'); }, (Math.random() * ran) + (stutter * 3));
    setTimeout(function () { newRandomTop('#blue-1__holder'); }, (Math.random() * ran) + (stutter * 0));
    setTimeout(function () { newRandomTop('#blue-2__holder'); }, (Math.random() * ran) + (stutter * 2));

}

var currentIndex = 0;
function newRandomTop(tar) {

  currentIndex++;
  currentIndex > 3 ? currentIndex = 0 : null;

  var width = 300;
  var minX = 25;
  var wiggle = 25;
  var step = (width - (minX * 2)) / 3;
  // var offsetX = (Math.random() * wiggle) - (wiggle / 2) + (step * (currentIndex)) + minX;
  var offsetX =  (Math.random() * (width - (minX * 2))) + minX;

  TweenLite.set(tar, { css: { y: -15, x: offsetX } });
  TweenLite.to(tar, ((Math.random() * 5) + 7.2), { css: { y: 265 }, ease: Power0.easeNone, onComplete: newRandomTop, onCompleteParams: [tar] });

}

/***
 *       __  _                ___          
 *      / /_(_)___ ___  ___  / (_)___  ___ 
 *     / __/ / __ `__ \/ _ \/ / / __ \/ _ \
 *    / /_/ / / / / / /  __/ / / / / /  __/
 *    \__/_/_/ /_/ /_/\___/_/_/_/ /_/\___/ 
 *                                         
 */

function initTimeline() {

  var animTiming = 0.35;
  var animStagger = 0.1;
  var framePause = 1.5;
  var animEasing = Power3.easeInOut;

  timeline
    .add('init')

  timeline
    .add('frame-1')
    .from('#logo__samsung', animTiming, { opacity: 0 }, 'frame1+=' + (animStagger * 0))
    .from('#frame-1__copy', animTiming, { opacity: 0 }, 'frame1+=' + (animStagger * 1))
    .from('#sled__holder', animTiming, { width: 0 }, 'frame1+=' + (animStagger * 2))
    .from('#box__sport-gear', animTiming, { opacity: 0 }, 'frame1+=' + (animStagger * 8))
    .from('#box__tablet', animTiming, { opacity: 0 }, 'frame1+=' + (animStagger * 7))
    .from('#box__camera', animTiming, { opacity: 0 }, 'frame1+=' + (animStagger * 7))

    .add('frame2', '+=' + framePause)
    .to('#frame-1__copy', animTiming, { opacity: 0 }, 'frame2+=' + (animStagger * 0))
    .from('#frame-2__copy', animTiming, { opacity: 0 }, 'frame2+=' + (animStagger * 0))

    .to('#box__sport-gear--close', animTiming * 4, { y: 25, x: 100, rotation: 35, ease: Power1.easeIn }, 'frame2+=' + (animStagger * 0))
    .to('#box__tablet--close', animTiming * 4, { y: 20, x: 250, rotation: 30, ease: Power1.easeIn }, 'frame2+=' + (animStagger * 0))
    .to('#box__camera--close', animTiming * 5, { y: 0, x: 30, rotation: 20, ease: Power1.easeOut }, 'frame2+=' + (animStagger * 0))

    .add('frame3', '+=' + framePause * 0)
    .from('#cta', animTiming, { opacity: 0 }, 'frame3+=' + (animStagger * 0))
    .from('#reload', animTiming, { opacity: 0 }, 'frame3+=' + (animStagger * 6))
    .from('#disclaimer', animTiming, { opacity: 0 }, 'frame3+=' + (animStagger * 6))

  if (timeline.totalDuration() > 15) {
    console.log('Duration is more than 15seconds, scaling down time');
    timeline.timeScale(timeline.totalDuration() / 15);
  }

}

/***
 *        __                    ____              
 *       / /_  ____ _____  ____/ / /__  __________
 *      / __ \/ __ `/ __ \/ __  / / _ \/ ___/ ___/
 *     / / / / /_/ / / / / /_/ / /  __/ /  (__  ) 
 *    /_/ /_/\__,_/_/ /_/\__,_/_/\___/_/  /____/  
 *                                                
 */

function clickThroughHandler(evt) {
  evt.stopPropagation();
  EB.clickthrough();
}

function replayClickHandler(evt) {
  evt.stopPropagation();
  timeline.restart();
}