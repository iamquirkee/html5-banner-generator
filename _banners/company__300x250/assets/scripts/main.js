/***
 *                       _       __    __         
 *     _   ______ ______(_)___ _/ /_  / /__  _____
 *    | | / / __ `/ ___/ / __ `/ __ \/ / _ \/ ___/
 *    | |/ / /_/ / /  / / /_/ / /_/ / /  __(__  ) 
 *    |___/\__,_/_/  /_/\__,_/_.___/_/\___/____/  
 *     
 *  Don't edit unless you know what you're doing
 *                                            
 */

var timeline = new TimelineMax();
var element = {};

/*
  Setup elements for use
  -------------------- */
var t_Elements = DOM_UTILS.getAllIdElements(document.getElementById('main'));

DOM_UTILS.getAllIds(document.getElementById('main')).forEach(function(val, ind) {
  element[val] = t_Elements[ind];
})

/***
 *               __      __  ____                   
 *        ____  / /___ _/ /_/ __/___  _________ ___ 
 *       / __ \/ / __ `/ __/ /_/ __ \/ ___/ __ `__ \
 *      / /_/ / / /_/ / /_/ __/ /_/ / /  / / / / / /
 *     / .___/_/\__,_/\__/_/  \____/_/  /_/ /_/ /_/ 
 *    /_/ 
 * 
 *  Remove unused code from here
 *                                           
 */

/*
  Sizmek API
  --------------------*/
adkit.onReady(onInit);

/***
 *        _       _ __ 
 *       (_)___  (_) /_
 *      / / __ \/ / __/
 *     / / / / / / /_  
 *    /_/_/ /_/_/\__/  
 *                     
 */

function onInit() {

  // Setup Clicks
  element['cta'].addEventListener('click', clickThroughHandler);
  element['clicktag'].addEventListener('click', clickThroughHandler);
  element['reload'].addEventListener('click', replayClickHandler);

  // Init Copies
  initCopies();

  // Init timeline
  initTimeline();

  // Show banner
  document.getElementById('main').style.display = 'block';

}

function initCopies() {
  // Setup Copies
  element['frame-1a-text'].innerHTML = adkit.getSVData("frame-1a-text");
  element['frame-1b-text'].innerHTML = adkit.getSVData("frame-1b-text");
  element['frame-1c-text'].innerHTML = adkit.getSVData("frame-1c-text");
  element['frame-1d-text'].innerHTML = adkit.getSVData("frame-1d-text");
  element['frame-2a-text'].innerHTML = adkit.getSVData("frame-2a-text");
  element['frame-2b-text'].innerHTML = adkit.getSVData("frame-2b-text");
  element['frame-2c-text'].innerHTML = adkit.getSVData("frame-2c-text");
  element['frame-2d-text'].innerHTML = adkit.getSVData("frame-2d-text");
  element['frame-3a-text'].innerHTML = adkit.getSVData("frame-3a-text");
  element['frame-3b-text'].innerHTML = adkit.getSVData("frame-3b-text");
  element['frame-3c-text'].innerHTML = adkit.getSVData("frame-3c-text");
  element['frame-3d-text'].innerHTML = adkit.getSVData("frame-3d-text");
  element['cta'].innerHTML = adkit.getSVData("cta");
}

/***
 *       __  _                ___          
 *      / /_(_)___ ___  ___  / (_)___  ___ 
 *     / __/ / __ `__ \/ _ \/ / / __ \/ _ \
 *    / /_/ / / / / / /  __/ / / / / /  __/
 *    \__/_/_/ /_/ /_/\___/_/_/_/ /_/\___/ 
 *                                         
 */

function initTimeline() {
  timeline
    .add('init')
    .from('#logo-samsung', 0.2, {
      opacity: 0
    }, 'init')
    .from('#logo-note-8', 0.2, {
      opacity: 0
    }, 'init')
    .from('#phone-mask', 0.3, {
      opacity: 0,
      onStart: function() {
        element['reload'].style.display = 'none'
      }
    }, 'init+=0.1')

  timeline
    .add('start')
    .from('#spen', 0.5, {
      opacity: 0
    }, 'start')
    .from('#wave__holder--1', 0.4, {
      width: 0,
      scaleY: 0.2
    }, 'start')
    .from('#wave__holder--2', 0.3, {
      width: 0,
      scaleY: 0.2
    }, 'start+=0.1')
    .from('#wave__holder--1', 0.5, {
      opacity: 0
    }, 'start')
    .from('#wave__holder--2', 0.4, {
      opacity: 0
    }, 'start+=0.1')
    .to('#spen', 0.4, {
      bezier: {
        values: [{
            x: 0,
            y: 0
          },
          {
            x: 50,
            y: 0
          },
          {
            x: 60,
            y: -15
          }
        ],
        type: "soft"
      }
    }, 'start');

  timeline
    .from('#frame-1-text', .3, {
      opacity: 0
    }, 'start+=0.25')
    .to('#frame-1-text', .3, {
      opacity: 0
    }, 'start+=2.75')
    .from('#frame-2-text', .3, {
      opacity: 0
    }, 'start+=2.75')
    .to('#frame-2-text', .3, {
      opacity: 0
    }, 'start+=4.75')
    .from('#frame-3-text', .3, {
      opacity: 0
    }, 'start+=4.75')
    .from('#cta', .3, {
      opacity: 0
    }, 'start+=4.85')
    .from('#reload', .3, {
      opacity: 0,
      onStart: function() {
        element['reload'].style.display = 'block'
      }
    }, 'start+=4.95')

}

/***
 *        __                    ____              
 *       / /_  ____ _____  ____/ / /__  __________
 *      / __ \/ __ `/ __ \/ __  / / _ \/ ___/ ___/
 *     / / / / /_/ / / / / /_/ / /  __/ /  (__  ) 
 *    /_/ /_/\__,_/_/ /_/\__,_/_/\___/_/  /____/  
 *                                                
 */

function clickThroughHandler(evt) {
  evt.stopPropagation();
  // EB.clickthrough();
}

function replayClickHandler(evt) {
  evt.stopPropagation();
  timeline.restart();
}