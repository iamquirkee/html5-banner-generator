const defaults = {
  sizeLimit: 200 * 1000, // IAB 200kb limit
}

const paths = {
  html: './index.html',
  images: '/assets/images/**/*',
  styles: '/assets/styles/**/*',
  scripts: '/assets/scripts/**/*'
}

const folders = {
  zip: '_dist-min',
  dist: '_dist'
}

/* 
  Export
  -------------------- */
module.exports = {
  defaults: defaults,
  paths: paths,
  folders: folders
}