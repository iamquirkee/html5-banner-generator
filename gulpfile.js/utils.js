const fs = require('fs-jetpack');
const path = require('path');
const gutil = require('gulp-util');
const config = require('./config.js');

/*
  Utils
  -------------------- */
const utils = {

  getFolders: function(dir) {
    var folders = [];
    if (fs.inspectTree(dir) != undefined) {
      fs.inspectTree(dir).children.forEach(function(folder) {
        if (folder.type === 'dir') { folders.push(folder.name); }
      });
    }
    return folders;
  },

  getBanners: function() {
    var bannerList = [];
    // return only folders with dimensions in label
    var banners = utils.getFolders('_banners');
    banners.forEach(function(item) {
      if (item.match(config.sizeRegExp)) {
        bannerList.push(item);
      }
    });
    return bannerList;
  },

  getBannersFrom: function(folder) {
    var bannerList = [];
    // return only folders with dimensions in label
    var banners = utils.getFolders(folder);
    banners.forEach(function(item) {
      if (item.match(config.sizeRegExp)) {
        bannerList.push(item);
      }
    });
    return bannerList;
  },

  getBannerDetails: function(folderName = '') {
    let name = folderName.split('__');
    let returnDimensions = {
      folderName: folderName,
      name: 'invalid-format',
      height: 0,
      width: 0,
      get formatted() {
        return `${this.width}x${this.height}`;
      }
    };
    // Check folder format
    if (name.length == 2) {

      returnDimensions.name = name[0];

      // Check dimensions
      let dimensions = name[1].split('x');
      let width = parseInt(dimensions[0]);
      let height = parseInt(dimensions[1]);

      width > 0 ? width = returnDimensions.width = width : returnDimensions.width = 0;
      height > 0 ? height = returnDimensions.height = height : returnDimensions.height = 0;

    }
    return returnDimensions;
  },

  error: {
    title: function(title) {
      return gutil.colors.bgRed.white.bold(`\n   ${title}   \n`);
    },
    listItem: function(type, description) {
      return gutil.colors.red('✘ ') + gutil.colors.bold(type) + ': ' + description + '\n';
    },
    itemDescription: function(description) {
      return gutil.colors.gray('    - ' + description + '\n');
    }
  },

  success: {
    title: function(title) {
      return gutil.colors.bgGreen.white.bold(`\n   ${title}   \n`);
    },
    listItem: function(type, description) {
      return gutil.colors.green('✔ ') + gutil.colors.bold(type) + ': ' + description + '\n';
    },
    itemDescription: function(description) {
      return gutil.colors.gray('    - ' + description + '\n');
    }
  },

  caution: {
    title: function(title) {
      return gutil.colors.bgYellow.black.bold(`\n   ${title}   \n`);
    },
    listItem: function(type, description) {
      return gutil.colors.yellow('- ') + gutil.colors.bold(type) + ': ' + description + '\n';
    },
    itemDescription: function(description) {
      return gutil.colors.gray('    - ' + description + '\n');
    }
  }

}

module.exports = utils;