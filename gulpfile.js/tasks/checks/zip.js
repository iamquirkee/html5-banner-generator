
module.exports = function (gulp, plugins, config, utils) {

  gulp.task('check:zip', false, function () {

    const colors = plugins.gutil.colors;

    /*
      Errors
      -------------------- */
    let errors = [];

    plugins.fs.inspectTree('./_dist-min').children.forEach(function (zipFile) {
      if (zipFile.type !== 'file') { return; }
      if (/(\w+)-all\(\d+\)/.test(zipFile.name)) { return; } // ignore larger collection zip file
      if (zipFile.size > config.defaults.sizeLimit) {
        errors.push(utils.error.listItem(zipFile.name, 'file size is too big'));
        errors.push(utils.error.itemDescription(`File Size Limit: ${colors.cyan(config.defaults.sizeLimit)}bytes`));
        errors.push(utils.error.itemDescription(`File Size: ${colors.cyan(zipFile.size)}bytes`));
      };
    });

    if (errors.length > 0) {
      console.log(`${utils.error.title('Zip')}${errors.join('')} `);
      process.exit();
    }

  })

};