module.exports = function (gulp, plugins, config, utils) {
  gulp.task('check:fallback', false, function () {

    const banners = utils.getBanners();
    const colors = plugins.gutil.colors;

    /*
      Errors
      -------------------- */
    let errors = [];

    banners.forEach(function (banner) {

      const folder = `_banners/${banner}`;
      const details = utils.getBannerDetails(banner);
      const image = plugins.fs.find(folder, { matching: ['fallback.*'] });

      // Check if dimensions are sound
      if (details.height == 0 || details.width == 0) {
        errors.push(utils.error.listItem(details.folderName, 'cannot parse banner dimensions'));
        if (details.height == 0) {
          errors.push(utils.error.itemDescription(`Banner ${colors.cyan('height')} is invalid please check folder name`));
        }
        if (details.width == 0) {
          errors.push(utils.error.itemDescription(`Banner ${colors.cyan('width')} is invalid please check folder name`));
        }
      }

      if (image.length == 0) {
        // No fallback Image  
        errors.push(utils.error.listItem(details.folderName, 'missing fallback image'));
        errors.push(utils.error.itemDescription(`Missing fallback image`));
      } else if (image.length != 1) {
        // Too many fallbacks
        errors.push(utils.error.listItem(details.folderName, 'multiple fallback images (' + image.length + ')'));
        errors.push(utils.error.itemDescription(`Too many fallback images`));
      } else {

        const fallbackImage = plugins.path.parse(image[0]);
        const fallbackDimensions = plugins.sizeOf(image[0]);

        // Invalid Format
        if (!fallbackImage.ext.match(/jpg|gif|png/)) {
          errors.push(utils.error.listItem(details.folderName, 'Invalid fallback image'));
          errors.push(utils.error.itemDescription(`Allowed types ${colors.cyan('JPG, GIF, PNG')}, `));
        } else {
          // Invalid Size
          if(
            fallbackDimensions.height != details.height ||
            fallbackDimensions.width != details.width
          ) {
            errors.push(utils.error.listItem(details.folderName, 'fallback does not match banner size'));
            errors.push(utils.error.itemDescription(`Banner Size: ${colors.cyan(details.width)}x${colors.cyan(details.height)}`));
            errors.push(utils.error.itemDescription(`Fallback Image Size: ${colors.cyan(fallbackDimensions.width)}x${colors.cyan(fallbackDimensions.height)}`));
          }
        }

      }

    });

    if (errors.length > 0) {
      console.log(`${utils.error.title('Error(s)')}${errors.join('')} `);
      process.exit();
    }

  });
};