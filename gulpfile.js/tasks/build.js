var strip = require('gulp-strip-comments');

module.exports = function (gulp, plugins, config, utils) {

  gulp.task('build', 'Builds banners', ['check:fallback', 'clean'], function (done) {
    plugins.sequence(
      ['copy:html', 'copy:sizmekConfig', 'build:images', 'build:styles', 'build:scripts'],
      'zip',
      'check:zip',
      done
    );
  });

  gulp.task('copy:html', false, function (done) {

    const banners = utils.getBanners('/_banners');
    let doneCounter = banners.length;

    banners.map(function (banner) {
      return gulp
        .src('_banners/' + banner + '/*.html')
        .pipe(strip())
        .pipe(gulp.dest('./' + config.folders.dist + '/' + banner))
        .on('end', function () {
          doneCounter--;
          if (doneCounter == 0) {
            done();
          }
        })
    });

  })

  gulp.task('copy:sizmekConfig', false, function (done) {

    const banners = utils.getBanners('/_banners');
    let doneCounter = banners.length;

    banners.map(function (banner) {
      return gulp
        .src('_banners/' + banner + '/config.js')
        .pipe(gulp.dest('./' + config.folders.dist + '/' + banner))
        .on('end', function () {
          doneCounter--;
          if (doneCounter == 0) {
            done();
          }
        })
    });

  })

  gulp.task('build:images', false, function (done) {

    const banners = utils.getBanners('/_banners');
    let doneCounter = banners.length;

    banners.map(function (banner) {

      return plugins.streamqueue({
        objectMode: true
      },
        gulp.src('_banners/' + banner + '/fallback.*'),
        gulp.src('_banners/' + banner + config.paths.images),
        gulp.src('_shared/images/**/*')
      )
        .pipe(plugins.newer('./' + config.folders.dist + '/' + banner))
        .pipe(plugins.image({
          mozjpeg: false,
          jpegRecompress: false,
          guetzli: false
        }))
        .pipe(gulp.dest('./' + config.folders.dist + '/' + banner))
        .on('end', function () {
          doneCounter--;
          if (doneCounter == 0) {
            done();
          }
        })

    })

  })

  gulp.task('build:styles', false, function (done) {

    const banners = utils.getBanners('/_banners');
    let doneCounter = banners.length;

    banners.map(function (banner) {

      return plugins.streamqueue({
        objectMode: true
      },
        gulp.src('./_shared/styles/**/*.scss'),
        gulp.src('./_banners/' + banner + config.paths.styles),
      )
        .pipe(plugins.concat('main.scss'))
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer({
          browsers: ['ie >= 9', 'safari >= 8'],
          cascade: false
        }))
        .pipe(gulp.dest('./' + config.folders.dist + '/' + banner))
        .on('end', function () {
          doneCounter--;
          if (doneCounter == 0) {
            done();
          }
        })

    });

  })

  gulp.task('build:scripts', false, function (done) {

    const banners = utils.getBanners('/_banners');
    let doneCounter = banners.length;

    banners.map(function (banner) {

      return plugins.streamqueue({
        objectMode: true
      },
        gulp.src('_shared/scripts/**/*'),
        gulp.src('_banners/' + banner + config.paths.scripts)
      )
        .pipe(plugins.concat('main.js'))
        .pipe(strip())
        .pipe(gulp.dest('./' + config.folders.dist + '/' + banner))
        .on('end', function () {
          doneCounter--;
          if (doneCounter == 0) {
            done();
          }
        })

    });

  })

  gulp.task('zip', false, ['clean:zip'], function (done) {

    const banners = utils.getBannersFrom(config.folders.dist);
    let doneCounter = banners.length;

    banners.map(function (banner) {
      return gulp
        .src('./' + config.folders.dist + '/' + banner + '/**/*')
        .pipe(plugins.zip(banner + '.zip'))
        .pipe(gulp.dest('./' + config.folders.zip))
        .on('end', function () {
          doneCounter--;
          if (doneCounter == 0) {
            done();
          }
        });
    });

  })

};