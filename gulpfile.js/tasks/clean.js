module.exports = function (gulp, plugins, config, utils) {

  gulp.task('clean', false, ['clean:zip'], function (done) {
    return plugins.del('./' + config.folders.dist + '/*');
  })

  gulp.task('clean:zip', false, function (done) {
    return plugins.del('./' + config.folders.zip + '/*');
  })

}