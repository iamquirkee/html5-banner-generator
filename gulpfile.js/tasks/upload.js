const ftp = require('vinyl-ftp');
const gutil = require('gulp-util');
const env = require('dotenv').config();

const ftpHost = process.env.CONN_FTP;
const ftpUser = process.env.CONN_USER;
const ftpPassword = process.env.CONN_PASS;
const ftpFolder = process.env.CONN_FOLDER;

module.exports = function (gulp, plugins, config, utils) {

  gulp.task('upload:viewer', ['build'], function (done) {

    let folders = [];
    // Get folders from dist
    plugins.fs.inspectTree('_dist').children.forEach(function (folder) {
      if (folder.type === 'dir') { folders.push(utils.getBannerDetails(folder.name).folderName); }
    });

    const conn = ftp.create({
      host: ftpHost,
      user: ftpUser,
      password: ftpPassword,
      parallel: 10,
      log: function (log) { }
    })
    return gulp
      .src(
      ['./_banner-support-files/index.html'], {
        base: './_banner-support-files',
        buffer: false
      }
      )
      .pipe(plugins.replace(/<!-- {inject:banners} -->/ig, `<script> var loadedBanners = [${folders.map(function(val){return `"${val}"`}).join()}];</script>`))
      .pipe(conn.dest(ftpFolder + '/_dist/')) // Upload to folder

  });

  gulp.task('upload', ['upload:viewer'], function (done) {

    let hasErrors = false;

    const banners = utils.getBannersFrom('./_dist');

    if (banners.length > 0) {

      // Start upload log
      let tLogs = [];
      // Create a space from previous log
      banners.forEach(function (banner) { tLogs.push(utils.caution.listItem(banner, `Uploading banner to ${ftpFolder}`)); });
      banners.forEach(function (banner) { tLogs.push(utils.caution.listItem(banner + '.zip', `Uploading zipped banner to ${ftpFolder}`)); });
      console.log(`${utils.caution.title('Uploading Files')}${tLogs.join('')}`);

      const conn = ftp.create({
        host: ftpHost,
        user: ftpUser,
        password: ftpPassword,
        parallel: 10,
        log: function (log) { }
      })

      return gulp
        .src(
        ['./_dist/**', './_dist-min/**'], {
          base: '.',
          buffer: false
        }
        )
        .pipe(plugins.plumber({
          errorHandler: function (err) {
            hasErrors = true;
            console.log(`${utils.error.title('Failed to upload')}${utils.error.listItem('FTP', `${err}`)}`);
          }
        }))
        .pipe(conn.newer(ftpFolder)) // Only upload newer
        .pipe(conn.dest(ftpFolder)) // Upload to folder
        .on('end', function () {
          if (!hasErrors) {
            console.log(`${utils.success.title('Uploaded Files')}${utils.success.listItem('Success', `Files are uploaded to ${ftpHost}${ftpFolder}`)}`);
            banners.forEach(function (banner) {
              console.log(`http://${ftpHost}${ftpFolder}/_dist/${banner}`);
              console.log(`http://${ftpHost}${ftpFolder}/_dist-min/${banner}.zip`);
            })
            console.log('');
          }
        })

    } else {

      console.log(`${utils.error.title('No files')}${utils.error.listItem('Error', `No files found in _dist folder`)}`);

    }

  });

};