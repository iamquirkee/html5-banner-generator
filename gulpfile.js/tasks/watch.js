const browserSync = require('browser-sync').create();
const browserSyncReuseTab = require('browser-sync-reuse-tab')(browserSync);

const watchErrorHandler = (err) => {
  gutil.log(
    gutil.colors.reset('❌ ') +
    gutil.colors.bgRed.white(' ' + err.code + ' ') +
    gutil.colors.reset(' 📁 ') +
    gutil.colors.magenta(' ' + err.filename)
  );
};

module.exports = function (gulp, plugins, config, utils) {

  gulp.task('watch', 'Watches specified banner', ['build'], function (done) {

    const colors = plugins.gutil.colors;

    /*
      Errors
      -------------------- */
    let errors = [];

    let folders = [];
    // Get folders from dist
    plugins.fs.inspectTree('_dist').children.forEach(function (folder) {
      if (folder.type === 'dir') { folders.push(utils.getBannerDetails(folder.name).folderName); }
    });

    browserSync.init({
      server: ['./_banner-support-files', './_dist'],
      open: false,
      rewriteRules: [{
        match: /<!-- {inject:banners} -->/ig,
        fn: function (match) {
          return `<script> var loadedBanners = [${folders.map(function(val){return `"${val}"`}).join()}];</script>`;
        }
      }
      ]
    }, browserSyncReuseTab);

    gulp.watch('_banners/**/*.html', ['watch:html']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/config.js', ['watch:sizmekConfig']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/assets/images/**/*', ['watch:images']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/assets/scripts/**/*', ['watch:scripts']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/assets/styles/**/*', ['watch:styles']).on('error', watchErrorHandler);
    gulp.watch('_shared/images/**/*', ['watch:images']).on('error', watchErrorHandler);
    gulp.watch('_shared/scripts/**/*', ['watch:scripts']).on('error', watchErrorHandler);
    gulp.watch('_shared/styles/**/*', ['watch:styles']).on('error', watchErrorHandler);

    if (errors.length > 0) {
      console.log(`${utils.error.title('Watch Error(s)')}${errors.join('')} `);
      process.exit();
    } else {
      done();
    }

  });

  gulp.task('watch:files', false, function () {
    gulp.watch('_banners/**/*.html', ['watch:html']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/config.js', ['watch:sizmekConfig']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/assets/images/**/*', ['watch:images']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/assets/scripts/**/*', ['watch:scripts']).on('error', watchErrorHandler);
    gulp.watch('_banners/**/assets/styles/**/*', ['watch:styles']).on('error', watchErrorHandler);
    gulp.watch('_shared/images/**/*', ['watch:images']).on('error', watchErrorHandler);
    gulp.watch('_shared/scripts/**/*', ['watch:scripts']).on('error', watchErrorHandler);
    gulp.watch('_shared/styles/**/*', ['watch:styles']).on('error', watchErrorHandler);
  })

  gulp.task('watch:sizmekConfig', false, ['copy:sizmekConfig'], function (done) {
    browserSync.reload();
    done();
  })

  gulp.task('watch:html', false, ['copy:html'], function (done) {
    browserSync.reload();
    done();
  })

  gulp.task('watch:images', false, ['build:images'], function (done) {
    browserSync.reload();
    done();
  })

  gulp.task('watch:scripts', false, ['build:scripts'], function (done) {
    browserSync.reload();
    done();
  })

  gulp.task('watch:styles', false, ['build:styles'], function (done) {
    browserSync.reload();
    done();
  })

}