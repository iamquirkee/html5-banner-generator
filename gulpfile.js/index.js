const gulp = require('gulp-help')(require('gulp'));
const tasks = require('require-dir')('./tasks', { recurse: true });
const config = require('./config');
const utils = require('./utils');
const plugins = require('gulp-load-plugins')({
  rename: {
    'gulp-util': 'gutil',
  }
});

plugins.fs = require('fs-jetpack');
plugins.sizeOf = require('image-size');
plugins.path = require('path');
plugins.del = require('del');
plugins.streamqueue = require('streamqueue');

/*
  Config
  -------------------- */
config.flags = require('minimist')(process.argv.slice(2));

/* 
  Setup tasks
  -------------------- */
function recursive(tasks) {
  for (let task in tasks) {
    if (tasks[task] instanceof Function) {
      tasks[task](gulp, plugins, config, utils);
    } else {
      if (typeof tasks[task] === 'object') {
        recursive(tasks[task]);
      }
    }
  }
}

recursive(tasks);


/*
gulp.task('default', 'Displays Help', function() {

    console.log('Test', args);
    plugins.gutil.log('test', 'test');
    // console.log(config);

})

gulp.task('version', 'prints the version.', [], function() {
    // ... 
}, {
    aliases: ['v', 'V'],
    options: {
        'env=prod': 'description of env, perhaps with available values',
        'key=val': 'description of key & val',
        'key': 'description of key'
    }
});

*/